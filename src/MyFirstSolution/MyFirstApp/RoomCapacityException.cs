﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyFirstApp
{
    public class RoomCapacityException : Exception
    {


        public int MaxAttendees { get; private set; }

        public int Attendees { get; private set; }


        public RoomCapacityException(int max, int attendees)
            : base(CreateMessage(max, attendees))
        {
            this.MaxAttendees = max;
            this.Attendees = attendees;
        }


        public RoomCapacityException(string msg)
            : base(msg)
        {

        }

        public RoomCapacityException(string msg, Exception inner) 
            : base(msg, inner)
        {

        }


        private static string CreateMessage(int max, int attendees)
        {
            return $"Room capacity exceeded. Maximum allowed is {max}, but {attendees} are enrolled.";
        }

    }
}
