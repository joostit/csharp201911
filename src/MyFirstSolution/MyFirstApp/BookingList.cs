﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyFirstApp
{

    public delegate void BudgetExceededEventHandler(object sender, BudgetExceededEventArgs e);

    class BookingList : List<Booking>
    {

        public event BudgetExceededEventHandler BudgetExceeded;

        public double BookingBudget { get; set; } = 500;

        public int TotalAttendees
        {
            get
            {
                int total = 0;
                foreach (var booking in this)
                {
                    total += booking.Attendees;
                }
                return total;
            }
        }


        public double TotalCosts
        {
            get
            {
                double total = 0;
                foreach (var booking in this)
                {
                    total += booking.Cost;
                }
                return total;
            }
        }


        public double AverageAttendees
        {
            get
            {
                double avg = 0;

                if (Count > 0)
                {
                    avg = TotalAttendees / (double)Count;
                }

                return avg;
            }
        }


        public List<string> UniqueRooms
        {
            get
            {
                List<string> uniqueNames = new List<string>();

                foreach (var booking in this)
                {
                    if (!uniqueNames.Contains(booking.RoomName))
                    {
                        uniqueNames.Add(booking.RoomName);
                    }
                }
                return uniqueNames;
            }
        }


        public void AddBooking(Booking booking)
        {
            base.Add(booking);

            if(TotalCosts > BookingBudget)
            {
                BudgetExceeded?.Invoke(this, new BudgetExceededEventArgs(BookingBudget, TotalCosts));
            }



            //Event:
            // In BookingList:
            //   1: Define event handler delegate
            //   2: Define event member with 'event' keyword
            //   3: Trigger/Raise the event
            // In Program:
            //   4: Implement event handler method according to the delegate from step 1
            //   5: Subscribe to event
        }


    }
}
