﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyFirstApp
{
    public class BudgetExceededEventArgs : EventArgs
    {

        public double Budget { get; private set; }

        public double TotalCosts { get; private set; }

        public BudgetExceededEventArgs(double budget, double totalCosts)
        {
            this.Budget = budget;
            this.TotalCosts = totalCosts;
        }


    }
}
