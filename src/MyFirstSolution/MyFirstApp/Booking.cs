﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyFirstApp
{
    class Booking
    {

        private const int MaxCapacity = 30;

        public string RoomName { get; set; }

        private int _attendees = 0;
        public int Attendees
        {
            get
            {
                return _attendees;
            }
            set
            {
                if (value > MaxCapacity)
                {
                    throw new RoomCapacityException(MaxCapacity, value);
                }
                _attendees = value;
            }
        }

        public RoomTypes RoomType { get; set; }

        public DateTime DateOfReservation { get; set; }

        public double Cost
        {
            get
            {
                return CalculateBookingCost(Attendees, RoomType);
            }
        }


        private double CalculateBookingCost(int attendees, RoomTypes roomType)
        {
            double retVal = 0;
            double costPerAttendee = 0;

            switch (roomType)
            {
                case RoomTypes.Internal:
                    costPerAttendee = RoomCostDefinitions.CostsInternal;
                    break;
                case RoomTypes.External:
                    costPerAttendee = RoomCostDefinitions.CostsExternal;
                    break;
                case RoomTypes.Luxurious:
                    costPerAttendee = RoomCostDefinitions.CostsLuxurious;
                    break;
                case RoomTypes.SpaceStation:
                    costPerAttendee = RoomCostDefinitions.CostsSpaceStation;
                    break;
                default:
                    throw new NotImplementedException($"No Case defined for {roomType} enum value");
            }

            retVal = attendees * costPerAttendee;

            return retVal;
        }

    }
}
