﻿using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Generic;
using System;

namespace MyFirstApp
{

    class Program
    {

        static void Main(string[] args)
        {
            BookingList bookings = new BookingList();

            bookings.BudgetExceeded += (sender, e) =>  Console.WriteLine($"Booking budget exceeded by {e.TotalCosts - e.Budget}!!");

            bool continueAsking;

            do
            {

                Console.WriteLine("Welcome at the planner tool");
                Console.WriteLine("Costs:");
                Console.WriteLine($"  Internal:      {RoomCostDefinitions.CostsInternal}");
                Console.WriteLine($"  External:      {RoomCostDefinitions.CostsExternal}");
                Console.WriteLine($"  Luxurious:     {RoomCostDefinitions.CostsLuxurious}");
                Console.WriteLine($"  Space Station: {RoomCostDefinitions.CostsSpaceStation}");

                try
                {
                    Booking newBooking = new Booking();

                    Console.WriteLine("");
                    Console.WriteLine("Please enter new booking: ");
                    newBooking.RoomName = AskRoomName();
                    newBooking.Attendees = AskNumberOfAttendees();
                    newBooking.RoomType = AskRoomType();
                    newBooking.DateOfReservation = AskReservationDate();
                    bookings.AddBooking(newBooking);


                    Console.WriteLine($"Room: {newBooking.RoomName}, has been booked for {newBooking.Attendees} people on {newBooking.DateOfReservation.ToShortDateString()}");
                    Console.WriteLine($"This booking will cost you {newBooking.Cost} Euro");

                }
                catch (RoomCapacityException ex)
                {
                    Console.WriteLine("Cannot create booking: " + ex.Message);
                }

                continueAsking = GetYesNoFromUser("Do you wish to add another reservation? (Y/N)");

            } while (continueAsking);



            Console.WriteLine($"Number of reservations: {bookings.Count}");
            Console.WriteLine($"Number of attendees: {bookings.TotalAttendees}");
            Console.WriteLine($"Average number of attendees: {Math.Round(bookings.AverageAttendees, 1)}");
            Console.WriteLine($"Total costs: {bookings.TotalCosts}");
            Console.WriteLine($"Number of unique rooms: {bookings.UniqueRooms.Count}");

            Console.ReadKey();
        }


        private static RoomTypes AskRoomType()
        {
            RoomTypes retVal = RoomTypes.Internal;
            bool isValidInput = false;

            Console.WriteLine("Please select Room Type: ");
            Console.WriteLine("  1 -> Internal");
            Console.WriteLine("  2 -> External");
            Console.WriteLine("  3 -> Luxurious");
            Console.WriteLine("  4 -> Space Station");

            do
            {
                var inputKey = Console.ReadKey(true);

                switch (inputKey.KeyChar)
                {
                    case '1':
                        retVal = RoomTypes.Internal;
                        isValidInput = true;
                        break;

                    case '2':
                        retVal = RoomTypes.External;
                        isValidInput = true;
                        break;

                    case '3':
                        retVal = RoomTypes.Luxurious;
                        isValidInput = true;
                        break;

                    case '4':
                        retVal = RoomTypes.SpaceStation;
                        isValidInput = true;
                        break;

                    default:
                        isValidInput = false;
                        Console.WriteLine("Please select a valid option (1, 2, 3)");
                        break;
                }
            } while (!isValidInput);

            return retVal;
        }

        private static DateTime AskReservationDate()
        {
            DateTime reservationDate;
            Console.WriteLine("Please enter reservation date yyyy-mm-dd");
            reservationDate = DateTime.Parse(GetInput(true));
            return reservationDate;
        }

        private static int AskNumberOfAttendees()
        {
            int attendees;
            Console.WriteLine("Please enter number of attendees");
            string attendeesAsString = GetInput(true);
            attendees = Int32.Parse(attendeesAsString);
            return attendees;
        }

        private static string AskRoomName()
        {
            string roomName;
            Console.WriteLine("Please enter room name");
            roomName = GetInput(false);
            return roomName;
        }

        static string GetInput(bool isRequired)
        {
            string input;
            bool noInputGiven = false;

            do
            {
                input = Console.ReadLine();
                noInputGiven = String.IsNullOrWhiteSpace(input);
            } while (noInputGiven && isRequired);

            return input;
        }


        static bool GetYesNoFromUser(string question)
        {
            bool userWantsToAddAnother = false;
            bool answerIsValid;
            do
            {
                Console.WriteLine(question);
                var answer = Console.ReadKey();

                switch (answer.KeyChar)
                {
                    case 'Y':
                    case 'y':
                        userWantsToAddAnother = true;
                        answerIsValid = true;
                        break;

                    case 'N':
                    case 'n':
                        userWantsToAddAnother = false;
                        answerIsValid = true;
                        break;

                    default:
                        answerIsValid = false;
                        break;
                }
            } while (!answerIsValid);

            return userWantsToAddAnother;
        }




    }

}

