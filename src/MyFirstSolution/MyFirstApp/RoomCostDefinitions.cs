﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyFirstApp
{
    static class RoomCostDefinitions
    {
        public const double CostsInternal = 0;
        public const double CostsExternal = 15;
        public const double CostsLuxurious = 150;
        public const double CostsSpaceStation = 700000;
    }
}
