﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VehicleLib
{
    public class Person
    {

        public string Name { get; set; }

        public DriversLicense DriversLicense { get; set; }

    }
}
