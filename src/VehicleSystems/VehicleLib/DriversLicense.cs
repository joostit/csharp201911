﻿namespace VehicleLib
{
    public class DriversLicense
    {

       
        public bool IsCarAllowed
        {
            get
            {
                return LicenseType >= DriversLicenseTypes.Car;
            }
        }


        public bool IsTouringcarAllowed
        {
            get
            {
                return LicenseType >= DriversLicenseTypes.Touringcar;
            }
        }

        public bool IsTruckAllowed
        {
            get
            {
                return (LicenseType >= DriversLicenseTypes.Truck);
            }
        }

        public DriversLicenseTypes LicenseType { get; set; }

    }
}