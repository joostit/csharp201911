﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VehicleLib.Propulsion;
using VehicleLib.Propulsion.Engines;

namespace VehicleLib.Vehicles
{
    public abstract class Vehicle
    {

        public IPowerSource PropulsionSource { get; private set; }

        public GearBox Gearbox { get; set; }


        public Vehicle(IPowerSource propulsionSource)
        {
            this.PropulsionSource = propulsionSource;
            this.Gearbox = new GearBox();

            propulsionSource.OutputPowerChanged += PropulsionSource_OutputPowerChanged;
        }

        private void PropulsionSource_OutputPowerChanged(object sender, OutputPowerChangedEventArgs e)
        {
            Gearbox.InputPower = e.OutputPower;
        }
    }
}
