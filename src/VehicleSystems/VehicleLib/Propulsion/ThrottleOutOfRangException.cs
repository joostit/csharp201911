﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VehicleLib.Propulsion
{
    public class ThrottleOutOfRangException : Exception
    {

        public int RangeMin { get; private set; }

        public int RangeMax { get; private set; }

        public int Actual { get; private set; }

        public ThrottleOutOfRangException(int min, int max, int actual)
            : base (CreateMessage(min, max, actual))
        {
            this.Actual = actual;
            this.RangeMin = min;
            this.RangeMax = max;
        }


        private static string CreateMessage(int min, int max, int actual)
        {
            return $"Throttle value {actual} cannot be below {min} or above {max}.";
        }

    }
}
