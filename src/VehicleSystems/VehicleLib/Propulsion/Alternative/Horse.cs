﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VehicleLib.Propulsion.Alternative
{
    public class Horse : PropulsionBase
    {
        public string Name { get; set; }

        public DateTime BirthDate { get; set; }


        public Horse(string name, DateTime birthDate)
        {
            this.Name = name;
            this.BirthDate = birthDate;
        }


        protected override double CalculateOutputPower()
        {
            throw new NotImplementedException();
        }
    }
}
