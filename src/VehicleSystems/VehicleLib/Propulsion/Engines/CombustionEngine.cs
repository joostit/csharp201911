﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VehicleLib.Propulsion.Engines
{
    public abstract class CombustionEngine : PropulsionBase
    {


        private double _Displacement;

        /// <summary>
        /// The engine displacement in liters
        /// </summary>
        public double Displacement
        {
            get
            {
                return _Displacement;
            }
            private set
            {
                _Displacement = value;
                UpdatePower();
            }
        }


        


        /// <summary>
        /// Constructor. Creates a new Engine object
        /// </summary>
        /// <param name="displacement">The engine displacement in liters</param>
        public CombustionEngine(double displacement)
        {
            Displacement = displacement;
        }





    }
}
