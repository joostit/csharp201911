﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VehicleLib.Propulsion.Engines
{
    public class GasolineEngine : CombustionEngine
    {


        public List<SparkPlug> SparkPlugs { get; set; } = new List<SparkPlug>();


        public GasolineEngine(double displacement)
            :base(displacement)
        {
            
        }


        protected override double CalculateOutputPower()
        {
            return Throttle * 0.9 * Displacement;
        }

    }
}
