﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VehicleLib.Propulsion.Engines
{
    public class DieselEngine : CombustionEngine
    {

        public bool IsPreglowing { get; set; }

        public DieselEngine(double displacement)
            :base(displacement)
        {

        }

        protected override double CalculateOutputPower()
        {
            return Throttle * 0.6 * Displacement;
        }

    }
}
