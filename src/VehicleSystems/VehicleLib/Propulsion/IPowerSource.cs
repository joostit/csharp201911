﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VehicleLib.Propulsion
{

    public interface IPowerSource
    {

        event OutputPowerChangedEventHandler OutputPowerChanged;

        double OutputPower { get; }

        int Throttle { get; set; }

        void Start();

        void Stop();

    }
}
