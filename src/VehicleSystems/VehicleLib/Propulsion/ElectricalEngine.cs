﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VehicleLib.Propulsion
{
    public class ElectricalEngine : PropulsionBase
    {


        public double MaxPower { get; private set; }


        public ElectricalEngine(double maxPower)
        {
            this.MaxPower = maxPower;
        }

        protected override double CalculateOutputPower()
        {
            return MaxPower * (Throttle / 100.0);
        }
    }
}
