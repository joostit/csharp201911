﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VehicleLib.Propulsion
{
    public abstract class PropulsionBase : IPowerSource
    {

        public const int ThrottleMin = 0;

        public const int ThrottleMax = 100;

        public event OutputPowerChangedEventHandler OutputPowerChanged;

        private double _OutputPower;

        /// <summary>
        /// The engine output power in HP
        /// </summary>
        public double OutputPower
        {
            get
            {
                return _OutputPower;
            }
            private set
            {
                if (OutputPower != value)
                {
                    _OutputPower = value;
                    OutputPowerChanged?.Invoke(this, new OutputPowerChangedEventArgs(OutputPower));
                }
            }
        }


        private int _throttle;

        /// <summary>
        /// The engine throttle in percentage (0 - 100)
        /// </summary>
        public int Throttle
        {
            get
            {
                return _throttle;
            }
            set
            {
                if ((value > ThrottleMax) || (value < ThrottleMin))
                {
                    throw new ThrottleOutOfRangException(ThrottleMin, ThrottleMax, value);
                }
                _throttle = value;
                UpdatePower();
            }
        }


        public void Start()
        {

        }

        public void Stop()
        {

        }


        protected void UpdatePower()
        {
            OutputPower = CalculateOutputPower();
        }



        protected abstract double CalculateOutputPower();
    }
}
