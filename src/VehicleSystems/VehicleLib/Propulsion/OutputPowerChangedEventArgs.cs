﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VehicleLib.Propulsion
{

    public delegate void OutputPowerChangedEventHandler(object sender, OutputPowerChangedEventArgs e);

    public class OutputPowerChangedEventArgs : EventArgs
    {

        public double OutputPower { get; private set; }

        public OutputPowerChangedEventArgs(double outputPower)
        {
            this.OutputPower = outputPower;
        }

    }
}
