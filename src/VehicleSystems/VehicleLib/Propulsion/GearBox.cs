﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VehicleLib.Propulsion
{
    public class GearBox
    {

        public event OutputPowerChangedEventHandler OutputPowerChanged;

        private double _Ratio = 1;

        public double Ratio
        {
            get
            {
                return _Ratio;
            }
            set
            {
                _Ratio = value;
                UpdateOutputPower();
            }
        }


        private double _InputPower;

        public double InputPower
        {
            get
            {
                return _InputPower;
            }
            set
            {
                _InputPower = value;
                UpdateOutputPower();
            }
        }

        private double _OutputPower;

        public double OutputPower
        {
            get
            {
                return _OutputPower;
            }
            private set
            {
                if (value != OutputPower)
                {
                    _OutputPower = value;
                    OutputPowerChanged?.Invoke(this, new OutputPowerChangedEventArgs(value));
                }
            }
        }

        private bool _IsClutchEngaged;

        public bool IsClutchEngaged
        {
            get
            {
                return _IsClutchEngaged;
            }
            set
            {
                _IsClutchEngaged = value;
                UpdateOutputPower();
            }
        }


        private void UpdateOutputPower()
        {
            if (IsClutchEngaged)
            {
                OutputPower = InputPower * Ratio;
            }
            else
            {
                OutputPower = 0;
            }
        }


    }
}
