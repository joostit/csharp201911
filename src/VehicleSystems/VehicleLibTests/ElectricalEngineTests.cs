﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VehicleLib.Propulsion;

namespace VehicleLibTests
{
    [TestClass]
    public class ElectricalEngineTests
    {


        [TestMethod]
        public void TestOutputPowerCalculation()
        {
            double maxPower = 120;
            int throttle = 70;
            double expected = maxPower * (throttle / 100.0);
            // Arrange
            ElectricalEngine testEngine = new ElectricalEngine(maxPower);

            // Act
            testEngine.Throttle = throttle;

            // Assert
            Assert.AreEqual(expected, testEngine.OutputPower, 0.00000001);
        }

        

        [TestMethod]
        public void TestOutputPowerChangedEventRaisedOnChange()
        {
            bool eventIsTriggered = false;

            // Arrange
            ElectricalEngine testEngine = new ElectricalEngine(10);
            testEngine.Throttle = 0;
            testEngine.OutputPowerChanged += (sender, e) => eventIsTriggered = true;

            // Act
            testEngine.Throttle = 10;

            // Assert
            Assert.IsTrue(eventIsTriggered);
        }


        [TestMethod]
        public void TestThrottleOutOfRangeThrown()
        {
            // Arrange
            PropulsionBase testEngine = new ElectricalEngine(10);

            // Act
            int actThrottle = 230;

            Assert.ThrowsException<ThrottleOutOfRangException>(() => testEngine.Throttle = actThrottle);
        }



    }
}
