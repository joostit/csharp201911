﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VehicleLib;

namespace VehicleLibTests
{
    [TestClass]
    public class DriversLicenseTests
    {

        [TestMethod]
        public void TestIsTruckAllowedForTruckReturnsTrue()
        {
            // Arrange
            DriversLicense license = new DriversLicense();
            // Act
            license.LicenseType = DriversLicenseTypes.Truck;
            // Assert
            Assert.IsTrue(license.IsTruckAllowed);
        }


        [TestMethod]
        public void TestIsTruckAllowedForNoneReturnsFalse()
        {
            // Arrange
            DriversLicense license = new DriversLicense();
            // Act
            license.LicenseType = DriversLicenseTypes.None;
            // Assert
            Assert.IsFalse(license.IsTruckAllowed);
        }

        [TestMethod]
        public void TestIsTruckAllowedForCarReturnsFalse()
        {
            // Arrange
            DriversLicense license = new DriversLicense();
            // Act
            license.LicenseType = DriversLicenseTypes.Car;
            // Assert
            Assert.IsFalse(license.IsTruckAllowed);
        }

        [TestMethod]
        public void TestIsTruckAllowedForTouringcarReturnsFalse()
        {
            // Arrange
            DriversLicense license = new DriversLicense();
            // Act
            license.LicenseType = DriversLicenseTypes.Touringcar;
            // Assert
            Assert.IsFalse(license.IsTruckAllowed);
        }

        [TestMethod]
        public void TestIsTouringCarAllowedForTouringcarReturnsTrue()
        {
            // Arrange
            DriversLicense license = new DriversLicense();

            // Act
            license.LicenseType = DriversLicenseTypes.Touringcar;
            
            // Assert
            Assert.IsTrue(license.IsTouringcarAllowed);
        }

        [TestMethod]
        public void TestIsTouringCarAllowedForTruckReturnsTrue()
        {
            // Arrange
            DriversLicense license = new DriversLicense();

            // Act
            license.LicenseType = DriversLicenseTypes.Truck;

            // Assert
            Assert.IsTrue(license.IsTouringcarAllowed);
        }

        [TestMethod]
        public void TestIsTouringCarAllowedForCarReturnsFalse()
        {
            // Arrange
            DriversLicense license = new DriversLicense();

            // Act
            license.LicenseType = DriversLicenseTypes.Car;

            // Assert
            Assert.IsFalse(license.IsTouringcarAllowed);
        }

        [TestMethod]
        public void TestIsTouringCarAllowedForNoneReturnsFalse()
        {
            // Arrange
            DriversLicense license = new DriversLicense();

            // Act
            license.LicenseType = DriversLicenseTypes.None;

            // Assert
            Assert.IsFalse(license.IsTouringcarAllowed);
        }

        [TestMethod]
        public void TestIsCarAllowedForNoneReturnsFalse()
        {
            // Arrange
            DriversLicense license = new DriversLicense();

            // Act
            license.LicenseType = DriversLicenseTypes.None;

            // Assert
            Assert.IsFalse(license.IsCarAllowed);
        }

        [TestMethod]
        public void TestIsCarAllowedForCarReturnsTrue()
        {
            // Arrange
            DriversLicense license = new DriversLicense();

            // Act
            license.LicenseType = DriversLicenseTypes.Car;

            // Assert
            Assert.IsTrue(license.IsCarAllowed);
        }

        [TestMethod]
        public void TestIsCarAllowedForTouringcarReturnsTrue()
        {
            // Arrange
            DriversLicense license = new DriversLicense();

            // Act
            license.LicenseType = DriversLicenseTypes.Touringcar;

            // Assert
            Assert.IsTrue(license.IsCarAllowed);
        }

        [TestMethod]
        public void TestIsCarAllowedForTruckReturnsTrue()
        {
            // Arrange
            DriversLicense license = new DriversLicense();

            // Act
            license.LicenseType = DriversLicenseTypes.Truck;

            // Assert
            Assert.IsTrue(license.IsCarAllowed);
        }

    }
}
