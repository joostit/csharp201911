﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VehicleLib;
using VehicleLib.Propulsion.Engines;

namespace VehicleLibTests
{
    [TestClass]
    public class EngineTests
    {

        [TestMethod]
        public void TestEngineDisplacementAtCreation()
        {
            // Arrange
            double displacement = 2.0;

            // Act
            GasolineEngine testEngine = new GasolineEngine(displacement);

            // Assert
            Assert.AreEqual(displacement, testEngine.Displacement, "Engine displacement is not set correctly by the constructor");
        }


    }
}
