﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VehicleLib.Propulsion;

namespace VehicleLibTests
{
    [TestClass]
    public class GearboxTests
    {


        [TestMethod]
        public void TestOutputPowerCalculation()
        {
            double ratio = 2.2;
            double inputPower = 25;
            double expectedOutput = ratio * inputPower;

            // Arrange
            GearBox testBox = new GearBox();
            testBox.IsClutchEngaged = true;

            // Act
            testBox.Ratio = ratio;
            testBox.InputPower = inputPower;

            // Assert
            Assert.AreEqual(expectedOutput, testBox.OutputPower, 0.00000001);

        }


        [TestMethod]
        public void TestOutputPowerChangedEvent()
        {
            bool eventRaised = false;

            // Arrange
            GearBox testBox = new GearBox();
            testBox.InputPower = 1;
            testBox.IsClutchEngaged = true;
            testBox.Ratio = 1;

            // Act
            testBox.OutputPowerChanged += (s, e) => eventRaised = true;
            testBox.InputPower = 10;

            // Assert
            Assert.IsTrue(eventRaised);
        }


    }
}
