﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeDemos
{
    class ExceptionDemo
    {
        public static void Run()
        {


            try
            {
                // Open file
                //RiskyMethod();  // Write stuff to file
                SendCompleteMailBox();
            }
            catch (DivideByZeroException exc)
            {
                Console.WriteLine("Divide by zero!! :-o");
                Console.WriteLine(exc.Message);
                Console.WriteLine(exc.ToString());
            }
            catch(ArgumentNullException exc)
            {
                Console.WriteLine("ArgNull exc");
            }
            finally
            {
                Console.WriteLine("Finally stuff etc...");
            }


        }



        private static void SendCompleteMailBox()
        {
            // Blablabal mailbox sending


            // catch OOM exc
            try
            {
                throw new OutOfMemoryException();
            }
            catch(OutOfMemoryException e)
            {
                throw new MailboxTooLargeException("Piet", 150, e);
                // Hier komt ie niet
                // ...
            }

        }

        private static void RiskyMethod()
        {
            int a = 1;
            int b = 0;

            int c = a / b;
        }

    }
}
