﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeDemos
{
    class LambdaDemo
    {


        private delegate void WorkDelegate(int amount);

        public void RunDemo()
        {
            List<string> strings = new List<string>()
            {
                "abc",
                "def",
                "etr",
                "mef",
                "orb",
                "ee",
                "oert",
                "ksiq"
            };


            Func<string, bool> myFilter = (input) =>
            {
                bool sizeOk = input.Length > 3;
                bool endCharOk = input.EndsWith("t");
                bool startCharOk = input.StartsWith("o");
                return sizeOk && endCharOk && startCharOk;
            };


            List<string> result = ApplyFilter(strings, myFilter);


            foreach (var resultString in result)
            {
                Console.WriteLine(resultString);
            }
        }





        private List<string> ApplyFilter(List<String> input, Func<string, bool> filter)
        {
            List<string> retVal = new List<string>();

            foreach (string val in input)
            {
                if (filter(val))
                {
                    retVal.Add(val);
                }
            }

            return retVal;
        }




        private void Comparison()
        {
            // Named method
            WorkDelegate methodVar = WorkMethod;


            // Anonymous
            WorkDelegate anonymousVar = delegate (int amount)
            {
                Console.WriteLine("Working from the anonymous method!!! " + amount);
            };


            // Lamba expression
            WorkDelegate lambdaVar = (amount) => Console.WriteLine("Working from the Lambda method!!!" + amount);


            methodVar.Invoke(5);
            anonymousVar.Invoke(6);
            lambdaVar.Invoke(7);
        }

        private void WorkMethod(int amount)
        {
            Console.WriteLine("Working from the method!!!");
        }

    }
}
