﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeDemos
{
    struct PersonStruct
    {
        public string Name;
        public int Age;

        public PersonStruct(string name, int age)
        {
            this.Name = name;
            this.Age = age;
        }
    }
}
