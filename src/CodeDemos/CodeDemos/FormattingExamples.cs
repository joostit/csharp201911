﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeDemos
{
    class FormattingExamples
    {


        public void formatExemples()
        {
            double myDouble = 123.456;

            string first = String.Format("{0:00000.000}", myDouble);
            string second = Math.Round(myDouble, 2).ToString();
            string third = myDouble.ToString().PadRight(9, '0');
            string fourth = String.Format("{0:0}", myDouble);
            string fifth = Truncate(myDouble, 2).ToString();

            int day = 8;
            int month = 2;
            int year = 2018;
            string dateString = String.Format("{0:00}-{1:00}-{2:0000}", day, month, year);

            Console.WriteLine(first);
            Console.WriteLine(second);
            Console.WriteLine(third);
            Console.WriteLine(fourth);
            Console.WriteLine(fifth);
            Console.WriteLine(dateString);
            Console.ReadKey();
        }


        private double Truncate(double value, int digits)
        {
            int order = (int)Math.Pow(10, digits);
            double tmp = ((int)(value * order)) / (double)order;

            return tmp;
        }

    }
}
