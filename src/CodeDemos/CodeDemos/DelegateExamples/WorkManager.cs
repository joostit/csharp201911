﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeDemos.DelegateExamples
{

    public delegate void WorkerDelegate(int amount);

    class WorkManager
    {

        private List<WorkerDelegate> workToDo = new List<WorkerDelegate>();

        public void PrepareWorkLoad()
        {
            Console.WriteLine("Preparing work load");
            
            Painter p = new Painter();
            p.OutOfPaint += OutOfPaintHandler;

            Carpenter c = new Carpenter();

            workToDo.Add(p.DoPainting);
            workToDo.Add(c.DoCarpent);
        }


        private void OutOfPaintHandler(object sender, EventArgs e)
        {
            Console.WriteLine("Painter needs paint. Manager is going to the Gamma");
        }


        public void ExecuteWork()
        {
            Console.WriteLine("Starting work");

            foreach (WorkerDelegate workerMethod in workToDo)
            {
                workerMethod.Invoke(28);
            }


            Console.WriteLine("Finished work");
        }

    }
}
