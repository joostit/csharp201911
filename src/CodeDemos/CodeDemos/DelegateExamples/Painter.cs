﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeDemos.DelegateExamples
{

    public delegate void OutOfPaintEventHandler(object sender, EventArgs e);

    class Painter
    {

        public event OutOfPaintEventHandler OutOfPaint;


        public void DoPainting(int amount)
        {
            Console.WriteLine($"The painter is painting {amount}!");

            if (amount > 10)
            {
                OutOfPaint?.Invoke(this, EventArgs.Empty);
            }

        }

    }
}
