﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeDemos.DelegateExamples
{
    class Carpenter
    {

        // This IS a verb!!!!
        public void DoCarpent(int amount)
        {
            Console.WriteLine($"Carpenter is carpenting {amount} :-D");
        }

    }
}
