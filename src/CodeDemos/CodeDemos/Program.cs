﻿using CodeDemos.DelegateExamples;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeDemos
{
    class Program
    {


        static void Main(string[] args)
        {


            LambdaDemo demo = new LambdaDemo();
            demo.RunDemo();

            Console.ReadKey();
        }

        private static void StringBuilderDemo()
        {
            string street = "Broadway";
            string number = " 28";
            string zipCode = " 002365";
            string city = " Springfield";
            string state = " Utah";
            string country = " U$A";

            // Bad for memory and performance
            string address = street
                + number
                + zipCode
                + city
                + state
                + country;


            // Better for memory and performance
            StringBuilder sb = new StringBuilder();
            sb.Append(street);
            sb.Append(number);
            sb.Append(zipCode);
            sb.Append(city);
            sb.Append(state);
            sb.Append(country);
            Console.WriteLine(sb.ToString());
        }

        private static void ValueVsReferenceTypeDemo()
        {
            PersonClass personC = new PersonClass();
            personC.Name = "Piet";
            personC.Age = 33;

            PersonStruct personS = new PersonStruct("Klaas", 37);

            HideName(personC);
            HideName(personS);

            Console.WriteLine($"Class person name: {personC.Name}");
            Console.WriteLine($"Struct person name: {personS.Name}");

            int number = 15;

            incrementNumber(number);
            Console.WriteLine(number);
        }

        private static void incrementNumber(int num)
        {
            num++;
        }


        private static void HideName(PersonClass p)
        {
            p.Name = "XXXXXX";
        }

        private static void HideName(PersonStruct p)
        {
            p.Name = "XXXXXX";
        }


        private static void DelegatesDemo()
        {
            WorkManager manager = new WorkManager();
            manager.PrepareWorkLoad();
            manager.ExecuteWork();
            
        }

        private static void QueueDemo()
        {
            Queue<string> myQueue = new Queue<string>();


            myQueue.Enqueue("stefan");
            myQueue.Enqueue("Klaas");
            myQueue.Enqueue("Jan");
            myQueue.Enqueue("pietje");

            Console.WriteLine(myQueue.Peek());
            Console.WriteLine(myQueue.Peek());
            Console.WriteLine(myQueue.Peek());
            Console.WriteLine(myQueue.Peek());
        }

        private static void ArrayDemo()
        {
            string[] oneDim = new string[7];

            int[,] twoDim = new int[4, 4];

            int[,,] threeDim = new int[4, 4, 4];

            int[,,,] fourdim = new int[10000, 10000, 10000, 100];

            Console.WriteLine(oneDim[1]);
            Console.WriteLine(twoDim[2, 3]);
            Console.WriteLine(threeDim[3, 4, 5]);
            Console.WriteLine(fourdim[8, 9, 4, 2]);
        }

        private static void ExceptionDemo()
        {
            try
            {
                CodeDemos.ExceptionDemo.Run();
            }
            catch (MailboxTooLargeException)
            {
                Console.WriteLine("MailBox too large");
            }
        }

        private static void CarDemoCode()
        {
            Car myCar = new Car(ConsoleColor.Black, "34-JAG-8");
            myCar.Accelerate(5);

            Car otherCar = new Car(ConsoleColor.Black, "CD-456-E");
            otherCar.Brake();

            Console.WriteLine(otherCar.FrontWheelWeight);

            List<Vehicle> myVehicles = new List<Vehicle>();

            for (int i = 0; i < 9; i++)
            {
                if (i % 2 == 0)
                {
                    Car newCar = new Car(ConsoleColor.Cyan);
                    newCar.License = $"NRAAAAA{i}";
                    myVehicles.Add(newCar);
                }
                else
                {
                    Bicycle bike = new Bicycle();
                    myVehicles.Add(bike);
                }

            }


            List<Object> l = new List<object>();
            l.Add(new Bicycle());
            l.Add(new Car(ConsoleColor.Yellow));
            l.Add(4);
            l.Add("Een string");
            l.Add(new OperatingSystem(PlatformID.Xbox, new Version("1.0")));


            foreach (var item in l)
            {
                Console.WriteLine(item.ToString());
            }


            Console.WriteLine($"Number of cars: {myVehicles.Count}");

            foreach (var theCar in myVehicles)
            {
                Console.WriteLine(theCar.ToString());
            }


            myCar.Sell();


            myCar.Brake();
            myCar.Brake(6);
        }
    }
}
