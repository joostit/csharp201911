﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeDemos
{
    public class Car : Vehicle
    {

        public ConsoleColor Color { get; set; }


        private string _license;
        public string License
        {
            get
            {
                return _license;
            }
            set
            {
                if (value == null)
                {
                    throw new ArgumentNullException(nameof(License), "The license plate cannot be null");
                }

                if (value.Length != 8)
                {
                    throw new InvalidOperationException("The license plate should always consist of 8 characters");
                }

                _license = value;
            }
        }


        public int FrontWheelWeight
        {
            get
            {
                return (int)Math.Round(weight / 3.0 * 2.0);
            }
        }


        public Car(ConsoleColor color, string license)
        {
            this.Color = color;
            this.License = license;
        }


        public Car(ConsoleColor color)
        {
            this.Color = color;
            this.License = "XX-XXX-X";
        }


        public void Brake(int brakePercentage, bool isEmergencyBrake = false, bool fastenSeatbelts = false)
        {
            // Braking code
            //
            // brakePercentage
            //
            //
        }

        public void Brake()
        {
            Brake(100);
        }

        public void Accelerate(int throttlePercentage)
        {
            // Accelerate a lot
        }


        public override string ToString()
        {
            if(License != null)
            {
                return License;
            }
            else
            {
                return base.ToString();
            }
        }


        public override void Sell()
        {
            Console.WriteLine("Flowers and terrible cake!!! :-D");
            base.Sell();
        }


    }
}
