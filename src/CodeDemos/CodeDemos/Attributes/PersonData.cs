﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace CodeDemos.Attributes
{
    [XmlRoot]
    public class PersonData
    {

        [XmlElement]
        public string Name { get; set; }

        [XmlElement]
        public DateTime BirthDate { get; set; }

        [XmlElement]
        public string City { get; set; }

        [XmlIgnore]
        public string Hobby { get; set; }

    }
}
