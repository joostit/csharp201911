﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeDemos
{
    public class MailboxTooLargeException : Exception
    {

        public string MailBoxName { get; private set; }

        public int MailBoxSize { get; private set; }

        public DateTime TimeStamp { get; private set; }

        public MailboxTooLargeException(string mailboxName, int mailBoxSize, Exception inner)
            : base(CreateUsableMessage(mailboxName, mailBoxSize), inner)
        {
            this.MailBoxName = mailboxName;
            this.MailBoxSize = mailBoxSize;
            this.TimeStamp = DateTime.Now;
        }


        public MailboxTooLargeException(string msg) 
            : base(msg)
        {

        }

        public MailboxTooLargeException(string msg, Exception inner)
            : base(msg, inner)
        {

        }


        private static string CreateUsableMessage(string name, int size)
        {
            return $"Mailbox '{name}' is too large. Total size is {size} mb";
        }

    }
}
