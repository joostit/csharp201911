﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeDemos
{
    class Student
    {
        public string Name { get; set; }

        public DateTime BirthDate { get; set; }

        public int Id { get; set; }
    }
}
